package me.kulers;

import me.confuser.barapi.BarAPI;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;

import java.awt.*;

public class Events implements Listener {

    @EventHandler
    public void onPlayerDeathEvent(PlayerDeathEvent event) {
        Player player = event.getEntity();
        if (Main.teams.hasFlag(player)) {
            //spawn flag under retard who diedededededed
            Main.teams.dropFlag(player.getLocation().getBlock().getLocation(),Main.teams.getFlag(player));
            Main.teams.setFlag(player,"0");
        }
    }

    @EventHandler
    public void onPlayerDeathEvent(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if (Main.teams.hasFlag(player)) {
            //spawn flag under retard who diedededededed
            Main.teams.dropFlag(player.getLocation().getBlock().getLocation(),Main.teams.getFlag(player));
            Main.teams.setFlag(player,"0");
        }
    }



    @EventHandler
    public void onBlockBreakEvent(BlockBreakEvent event) {
        if(
                event.getBlock().getType() == Material.GLASS ||
                event.getBlock().getType() == Material.GREEN_STAINED_GLASS||
                event.getBlock().getType() == Material.BLUE_STAINED_GLASS ||
                event.getBlock().getType() == Material.RED_STAINED_GLASS ||
                event.getBlock().getType() == Material.YELLOW_STAINED_GLASS
        ){
            Block checkBlock = event.getBlock().getLocation().add(0,-1,0).getBlock();
            if(checkBlock.getType() == Material.BEACON){
                event.setCancelled(true);
                return;
            }

        }

        if(event.getBlock().getType() == Material.END_STONE){
            if(Main.teams.hasFlag(event.getPlayer()))
            {
                event.setCancelled(true);
                return;
            }
            Main.teams.pickFlag(event.getPlayer(), event.getBlock().getLocation());
            event.setDropItems(false);
        }

        if(event.getBlock().getType() == Material.DIAMOND_BLOCK){
            event.setCancelled(true);
        }

        if(event.getBlock().getType() == Material.BEACON){
            event.setCancelled(true);
            Block colorBlock = event.getBlock().getLocation().add(0,1,0).getBlock();
            String team = Main.teams.getTeamByBlock(colorBlock.getType());
            Player player = event.getPlayer();

            if(Main.teams.hasFlag(player)) return;

            if(team != null) {
                Main.teams.setFlag(player, team);
                colorBlock.setType(Material.GLASS);
            }
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {

        Player player = event.getPlayer();

        if (event.getHand() == EquipmentSlot.OFF_HAND) {
            return;
        }

        Material mat = player.getItemInHand().getType();


        if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Block block = event.getClickedBlock();

            if(block.getType() == Material.BEACON){
                //PLACE IN BEACON FLAG
                event.setCancelled(true);

                if(!Main.teams.hasFlag(player)) {
                    //return if player has no flag
                    return;
                }

                Block colorBlock = block.getLocation().add(0,1,0).getBlock();

                if(colorBlock.getType() != Material.GLASS) {
                    //return if beacon is taken
                    return;
                }

                TeamVariables team = Main.teams.getTeam(Main.teams.getFlag(player));
                colorBlock.setType(team.getMaterial());
                Main.teams.setFlag(player,"0");
                return;
                //loc.getBlock().setType(team.getMaterial());
//                System.out.println(Main.teams.getFlag(player));
//                block.setType(team.getMaterial());
//                Main.plugin.getServer().getWorld(player.getWorld().getName()).getBlockAt(loc).setType(team.getMaterial());
            }

            if(block.getType() == Material.OAK_WALL_SIGN){
                Sign sign = (Sign) block.getState();

                if(sign.getLine(0).equals(ChatColor.DARK_GREEN + "[TEAM]")) {
                    String teamName = sign.getLine(1);
                    player.sendMessage(Main.PREFIX_COLOR + Main.PREFIX + "You joined " + teamName + Main.PREFIX_COLOR +" team");
                    Main.teams.joinTeam(event.getPlayer(), teamName);
                    player.setScoreboard(Main.createScoreboard(player));
                    Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "pex user "+player.getName()+" group set "+teamName);


//                    new Thread(() ->
//                    {
//                        while(true){
//                            Robot r = null;
//                            try {
//                                r = new Robot();
//                            } catch (AWTException e) {
//                                e.printStackTrace();
//                            }
//
//
//                            for(Player p : Bukkit.getOnlinePlayers()){
//                                if(!p.equals(player)){
//                                    p.sendBlockChange(player.getLocation(), Material.BEACON, (byte)0);
//                                }
//                            }
//
//
//                            for (int xx = -1; xx <= 1; xx++)
//                            for (int yy = -1; yy <= 1; yy++)
//                            {
//                                Location diamondBlock = player.getLocation();
//                                diamondBlock.setY(player.getLocation().getY()-1);
//                                diamondBlock.setX(player.getLocation().getX()+xx);
//                                diamondBlock.setZ(player.getLocation().getZ()+yy);
//
//
//                                        for(Player p : Bukkit.getOnlinePlayers()){
//                                            if(!p.equals(player)){
//                                                p.sendBlockChange(diamondBlock, Material.DIAMOND_BLOCK, (byte)0);
//                                            }
//                                        }
//                                        System.out.println("TEST");
//                                    }
//                            r.delay(500);
//                            }
//
//                    }, "pizda").start();


                    //player.getWorld().playEffect(player.getLocation(), Effect.END_GATEWAY_SPAWN, 2);
//                    Location ll = player.getLocation();
//                    for(Player p : Bukkit.getOnlinePlayers()){
//                        Main.sendLightning(p, ll);
//                    }



//                    for(int i = 0; i < 30; i++){
//                        Location location = player.getLocation();
//                        location.setY(player.getLocation().getY() + 3 + i);
//                        //player.getWorld().spawnParticle(Particle., location, 99, 0, 1, 0, 0);
//                    }



                    //for(int i = 0; i < 30; i++)
                    //player.getWorld().spawnParticle(Particle.SPELL_MOB, player.getLocation().getX(),player.getLocation().getY() + i + 4,player.getLocation().getZ(), 25,0,0,0,new Particle.DustOptions(C));
                }

            }


            //player.sendMessage("Block id: " + block.getType().toString());

        }
    }

}
