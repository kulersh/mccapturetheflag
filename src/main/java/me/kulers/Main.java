package me.kulers;

import me.confuser.barapi.BarAPI;
import org.apache.commons.lang.WordUtils;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.util.Vector;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class Main extends JavaPlugin{

    public static final String PREFIX = "[Magebit]";
    public static final String PREFIX_COLOR = ChatColor.DARK_PURPLE.toString();

    public static Teams teams = new Teams();
    public static Main plugin;

    @Override
    public void onEnable() {
        // Plugin startup logic
        this.plugin = this;
        this.getServer().getPluginManager().registerEvents(new Events(), this);
        this.getCommand("magebit").setExecutor(new MagebitCommand());
        this.getCommand("team").setExecutor(new TeamCommand());


        new BukkitRunnable(){
            @Override
            public void run(){
                for(Player p : Bukkit.getOnlinePlayers()){
                    updateCompass(p);
                    drawTeam(p);

                    //check flag
                    if(teams.hasFlag(p)){
                        playParticle(p.getLocation(), teams.getFlag(p), new Vector(0,0,0),6);
                    }
                }

                for(Map.Entry<Location, String> p : Main.teams.dropedFlags.entrySet()){
                        playParticle(p.getKey(), p.getValue(), new Vector(0.5,-3,0.5), 20);
                }
            }
        }.runTaskTimer(this, 0L, 1L);

    }

    private void drawTeam(Player p) {
        int count = 12;
        Location location = p.getLocation();
        String team = teams.getTeam(p);
        if(team == null) return;
        Color tColor = teams.getTeam(team).getColor();


        for(int i = 0; i < count; i++){
            location.getWorld().spawnParticle(Particle.REDSTONE, location.getX()+(0.3*Math.cos((360/count*i))), location.getY() +3, location.getZ() + (0.3*Math.sin((360/count*i))), 0, 0.001, 1, 0, 1, new Particle.DustOptions(tColor, 1));
            //player.getWorld().spawnParticle(Particle.SPELL_MOB, location, 3, 0, 1, 0, 0);
        }
    }

    private void playParticle(Location locations,String flag, Vector vector, int am) {
        Color tColor = teams.getTeam(flag).getColor();
        Location location = locations;
        for(int i = 0; i < am; i++){
            location.getWorld().spawnParticle(Particle.REDSTONE, location.getX()+ vector.getX(), location.getY() +3 + (i*0.5) + vector.getY(), location.getZ() + vector.getZ(), 0, 0.001, 1, 0, 1, new Particle.DustOptions(tColor, 1));
            //player.getWorld().spawnParticle(Particle.SPELL_MOB, location, 3, 0, 1, 0, 0);
        }
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public static void updateCompass(Player player){

        String def = "N · · · ·15 · · · · 30· · · · ·45 · · · ·60 · · · · 75· · · ·E · · · 105 · · · ·120· · · · 135 · · · 150 · · · ·165· · · ·S · · · 195 · · · ·210· · · · 225 · · · 240 · · · ·255· · · ·W · · · 285 · · · ·300· · · · 315 · · · 330 · · · ·345· · · ·";
        Map<String, Double> degrees = new HashMap<>();
        Location location = player.getLocation();

        for (Player p : Bukkit.getOnlinePlayers()) {
            if (Main.teams.hasFlag(p) && !p.equals(player)) {
                degrees.put(
                        Main.teams.getTeam(p),
                        Math.toDegrees(Math.atan2(location.getX() - p.getLocation().getX(), location.getZ() - p.getLocation().getZ()))
                );
            }
        }

        for(Map.Entry<Location, String> p : Main.teams.dropedFlags.entrySet()){
            degrees.put(
                    p.getValue(),
                    Math.toDegrees(Math.atan2(location.getX() - p.getKey().getX()-0.5, location.getZ() - p.getKey().getZ()-0.5))
            );
        }

        def = wrapText(def,(int)((player.getLocation().getYaw()+360)/1.4754098360656), degrees);
        def = updateColors(def);
        BarAPI.setMessage(player,ChatColor.WHITE + def);
    }

    private static String updateColors(String def) {
        def = def.replace("N", ChatColor.YELLOW+"N"+ChatColor.WHITE);
        def = def.replace("W", ChatColor.YELLOW+"W"+ChatColor.WHITE);
        def = def.replace("S", ChatColor.YELLOW+"S"+ChatColor.WHITE);
        def = def.replace("E", ChatColor.YELLOW+"E"+ChatColor.WHITE);

        def = def.replace("X1", ChatColor.GREEN+"█"+ChatColor.WHITE);
        def = def.replace("X2", ChatColor.BLUE+"█"+ChatColor.WHITE);
        def = def.replace("X3", ChatColor.RED+"█"+ChatColor.WHITE);
        def = def.replace("X4", ChatColor.YELLOW+"█"+ChatColor.WHITE);
//        def = def.replace("·", ChatColor.GRAY+"·"+ChatColor.WHITE);
        return def;
    }

    private static String wrapText(String def, int num, Map<String, Double> degrees) {

        for (Map.Entry<String, Double> data : degrees.entrySet()) {

            double onePix = (float)(360 / (def.length()));
            double loc = data.getValue() * onePix;

            double inverse =  loc * -1;

            if(inverse <= 0){
                inverse = inverse + (360*onePix);
            }

            //System.out.println(inverse);
            int rep = (int)((245.0f/360)*inverse);
            System.out.println(rep);

//            def = def.substring(0, (int)inverse) + "X" + data.getKey() + def.substring((int)inverse);
            def = def.substring(0,rep)+"X"+data.getKey()+def.substring(Math.min(rep+1, def.length()));
            //def = def.substring(0, (int)(360/1.4754098360656*inverse)) + "X" + data.getKey() + def.substring((int)(360/1.4754098360656*inverse));
        }

        //System.out.println(def);
        //System.out.println(loc);

        for (int i = 0; i <= num+15; i++) {
//            String lastChar = def.substring(def.length() -1);
//            def = def.substring(0,def.length() -1);
//            def = lastChar + def;

            String firstChar = def.substring(0,1);
            def = def.substring(1, def.length());
            def =  def + firstChar;

        }
        int c = def.length() / 3;
        def =  def.substring(c, def.length()-3);

        return def;
    }

    public static Scoreboard createScoreboard(Player player) {
        Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective objective = scoreboard.registerNewObjective("scoreboard", "dummy");

        objective.setDisplayName("Magebit");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);


        Score first = objective.getScore(ChatColor.DARK_GREEN+"Team: " + teams.getTeam(player));
        Score second = objective.getScore("Hujzin");
        Score third = objective.getScore("te ar kaut ko");

        first.setScore(3);
        second.setScore(2);
        third.setScore(1);

        return scoreboard;
    }

    public static void sendLightning(Player p, Location l){
        Class<?> light = getNMSClass("EntityLightning");
        try {
            Constructor<?> constu =
                    light
                            .getConstructor(getNMSClass("World"),
                                    double.class, double.class,
                                    double.class, boolean.class, boolean.class);
            Object wh  = p.getWorld().getClass().getMethod("getHandle").invoke(p.getWorld());
            Object lighobj = constu.newInstance(wh, l.getX(), l.getY(), l.getZ(), false, false);

            Object obj =
                    getNMSClass("PacketPlayOutSpawnEntityWeather")
                            .getConstructor(getNMSClass("Entity")).newInstance(lighobj);

            sendPacket(p, obj);
        } catch (NoSuchMethodException | SecurityException |
                IllegalAccessException | IllegalArgumentException |
                InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    public static Class<?> getNMSClass(String name) {
        String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        try {
            return Class.forName("net.minecraft.server." + version + "." + name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void sendPacket(Player player, Object packet) {
        try {
            Object handle = player.getClass().getMethod("getHandle").invoke(player);
            Object playerConnection = handle.getClass().getField("playerConnection").get(handle);
            playerConnection.getClass().getMethod("sendPacket", getNMSClass("Packet"))
                    .invoke(playerConnection, packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
