package me.kulers;

import jdk.nashorn.internal.objects.annotations.Getter;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;

public class TeamVariables {
    private ChatColor chatColor;
    private Color color;
    private Material material;

    public TeamVariables(Color color, Material material, ChatColor chatColor) {
        this.color = color;
        this.material = material;
        this.chatColor = chatColor;
    }

    public Color getColor() {
        return color;
    }

    public Material getMaterial() {
        return material;
    }

    public ChatColor getChatColor() {
        return chatColor;
    }
}
