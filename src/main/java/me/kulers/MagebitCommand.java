package me.kulers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MagebitCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        System.out.println("#CMD#");
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if(args.length >= 1) {
                Main.teams.setFlag(player, args[0]);
            }
        }
        return false;
    }
}
