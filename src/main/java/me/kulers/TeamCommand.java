package me.kulers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeamCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        System.out.println("#CMD#");
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if(args.length >= 1) {
                if(Main.teams.teamColor.containsKey(args[0])){
                    Main.teams.joinTeam(player, args[0]);
                    player.sendMessage(Main.PREFIX_COLOR + Main.PREFIX + " You joined team "+Main.teams.teamColor.get(args[0]).getChatColor()+args[0]);
                }else{
                    player.sendMessage(Main.PREFIX_COLOR + Main.PREFIX + " That team does not exist!");
                }
            }
        }
        return false;
    }
}
