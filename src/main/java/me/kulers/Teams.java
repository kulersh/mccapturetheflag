package me.kulers;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class Teams {

    private HashMap<Player, String> team = new HashMap<>();
    private HashMap<Player, String> hasflag = new HashMap<>();
    public HashMap<String, TeamVariables> teamColor = new HashMap<>();
    public HashMap<Location, String> dropedFlags = new HashMap<>();

    Teams() {
        teamColor.put("1",new TeamVariables(Color.GREEN, Material.GREEN_STAINED_GLASS, ChatColor.GREEN));
        teamColor.put("2",new TeamVariables(Color.BLUE, Material.BLUE_STAINED_GLASS, ChatColor.BLUE));
        teamColor.put("3",new TeamVariables(Color.RED, Material.RED_STAINED_GLASS, ChatColor.RED));
        teamColor.put("4",new TeamVariables(Color.YELLOW, Material.YELLOW_STAINED_GLASS, ChatColor.YELLOW));
    }

    public void dropFlag(Location location, String flag){
        dropedFlags.put(location, flag);
        location.getBlock().setType(Material.END_STONE);
    }

    public void pickFlag(Player player, Location location){

        System.out.println(dropedFlags.size());
        Map.Entry<Location, String> s = null;
        for(Map.Entry<Location, String> p : dropedFlags.entrySet()){
            if(
                    p.getKey().getBlockX() == location.getBlockX() &&
                    p.getKey().getBlockY() == location.getBlockY() &&
                    p.getKey().getBlockZ() == location.getBlockZ()
            ){
                s = p;
            }

        }

        if(s != null) {
            Main.teams.setFlag(player, s.getValue());
            dropedFlags.remove(s.getKey());
        }

    }


    public void joinTeam(Player player, String teamName) {
        team.put(player, teamName);
    }

    public void setFlag(Player player, String flag) {
        if(!teamColor.containsKey(flag)){
            hasflag.remove(player);
            return;
        }
        hasflag.put(player, flag);
    }

    public String getFlag(Player player){
        if(!hasflag.containsKey(player)) return null;
        return hasflag.get(player);
    }

    public boolean hasFlag(Player player) {
        if(!hasflag.containsKey(player)) return false;
        return true;
    }

    public TeamVariables getTeam(String team) {
        return teamColor.get(team);
    }


    public String getTeam(Player player)
    {
        return team.get(player);
    }

    public String getTeamByBlock(Material type) {
        for (Map.Entry<String, TeamVariables> s : teamColor.entrySet()) {
            if(s.getValue().getMaterial() == type){
                return s.getKey();
            }
        }
        return null;
    }
}
